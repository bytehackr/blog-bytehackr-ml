---
description: Sandipan Roy
---

# $whoami

### Hi 👋, I'm Sandipan Roy                                               

**`Skills: Penetration Test | Threat Hunting | Security Research`**

An independent and self-motivated computer science graduate with 2 years of experience in Information Security. Completed M.Sc. in Computer Science with a demonstrated history of working in Web/Infra Security, Application Security & Vulnerability Management. Also having experience in penetration testing and reporting and provided a powerful combination of analysis, implementation, and customer support. I’m truly passionate about my work and always increase my knowledge. I also like to try new technologies and improve myself under each point of view.

👨💻 I’m currently working at [Red Hat](https://redhat.com/) as Associate Product Security Engineer.

📞 Reach me anytime at [**sandipan@redhat.com**](mailto:sandipan@redhat.com) or [**sandipan.roy@owasp.org**](mailto:sandipan.roy@owasp.org)\*\*\*\*

\*\*\*\*🔑 **GPG Key ID:** [**0x2895D0A52FEAA194**](https://raw.githubusercontent.com/ByteHackr/ByteHackr.github.io/master/sandipan_roy.asc)\*\*\*\*

## \`\`☎ `Reach Me Anytime` 🔑 

#### `LinkedIn:` [`https://www.linkedin.com/in/bytehackr/`](https://www.linkedin.com/in/bytehackr/)\`\`

#### `Twitter:` [`https://twitter.com/ByteHackr`](https://twitter.com/ByteHackr)\`\`

#### `GitHub:` [`https://github.com/ByteHackr`](https://github.com/ByteHackr)\`\`

#### `Google Scholar:` [`https://scholar.google.com/citations?hl=en&user=L09VGlUAAAAJ`](https://scholar.google.com/citations?hl=en&user=L09VGlUAAAAJ)\`\`

#### `OWASP Chapter:` [`https://owasp.org/www-chapter-berhampore/`](https://owasp.org/www-chapter-berhampore/)\`\`



